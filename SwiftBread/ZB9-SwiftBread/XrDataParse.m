//
//  XrDataParse.m
//  TasteEnjoy
//
//  Created by XuRui on 16/4/19.
//  Copyright © 2016年 XuRui. All rights reserved.
//

#import "XrDataParse.h"

@implementation XrDataParse

#pragma mark - ------------ 图片自适应 ---------------
+ (CGFloat)heghtWithImageName:(NSString *)imageName widthWithOriginal:(NSNumber *)widthOriginal heightWithOriginal:(NSNumber *)heightOriginal heightWithNow:(CGFloat)heightNow
{
    

    CGFloat widthNow = widthOriginal.integerValue * heightNow / heightOriginal.integerValue;


    return widthNow;
    

}


#pragma mark - ------------ 文字自适应 ---------------
+ (CGSize)heightWithText:(NSString *)text LabelWithSize:(CGSize)size fontText:(UIFont *)font

{
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font,NSFontAttributeName, nil];
    
    CGSize sizeText =  [text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    return sizeText;
    
}


@end
