//
//  TripModel.swift
//  ZB9-SwiftBread
//
//  Created by XuRui on 16/6/4.
//  Copyright © 2016年 XuRui. All rights reserved.
//

import UIKit

class TripModel: BaseModel {

    //属性
    var trip_text : String?
    var heightWithTripText : CGFloat?
    
    var avatar_m : String?
    
    var userName : String?

    var datetime : String?
    
    var valuev : NSNumber?
    
    var name : String?

    var waypoints : NSArray?

  
    
    
    //找到key
    
    override func setValue(value: AnyObject?, forKey key: String) {
        
       super.setValue(value, forKey: key)
        
        //自适应高度文字
        if key == "trip_text" {
            
            self.trip_text = value as? String
            
            let size : CGSize?
            
            size = XrDataParse.heightWithText(self.trip_text, labelWithSize: CGSizeMake(UIScreen.mainScreen().bounds.size.width - 20, 1000), fontText:UIFont.systemFontOfSize(16))
            
            
           self.heightWithTripText = size?.height
        }
        
        //数组里的照片
        if key == "waypoints" {
            
            let arr : NSMutableArray = NSMutableArray()
            
            for tempDic in value as! NSArray {
            
                let dic : NSDictionary = tempDic as! NSDictionary
                
                let model : PhotoModel = PhotoModel()
                
                model.setValuesForKeysWithDictionary(dic as! [String : AnyObject])
                
                arr.addObject(model)
                
           }
            
            self.waypoints = NSArray.init(array: arr)
            
        }
        
    }
    
    
    //找不到key
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
        if key == "trip" {
            
            self.name = "" + ((value?["name"])! as! String)

            self.datetime = value?["datetime"] as? String
        
            self.userName = value?["user"]!!["name"]as? String
            
            self.avatar_m = value?["user"]!!["avatar_m"]as? String
            
            self.valuev = value?["user"]!!["experience"]!!["level_info"]!!["value"]as? NSNumber
            
            self.userName = self.userName! + "LV." + (self.valuev?.stringValue)!
            
        }
        
    }
}
