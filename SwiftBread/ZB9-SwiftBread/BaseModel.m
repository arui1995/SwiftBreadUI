//
//  BaseModel.m
//  TasteEnjoy
//
//  Created by XuRui on 16/4/19.
//  Copyright © 2016年 XuRui. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel


#pragma mark - ------------ 重写初始化 ---------------
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        
        //kvc赋值
        [self setValuesForKeysWithDictionary:dictionary];
        
    }
    return self;
}

#pragma mark - ------------ 遍历构造器 ---------------
+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary
{
    
    id m = [[self alloc]initWithDictionary:dictionary];
    return m;
    

}

#pragma mark - ------------ 存字典的数组 -> 存model的数组 ---------------
+ (NSMutableArray *)modelHandleWithArrar:(NSArray *)arr
{
    
    //1.创建空数组
   NSMutableArray *mArr =[NSMutableArray array];
    
   //2.遍历传入的数组参数 得到字典信息
    for (NSDictionary *dic in arr) {
        
        id m = [[self class]modelWithDictionary:dic];
        [mArr addObject:m];
        
    }
    return mArr;
}


#pragma mark - ------------ 当model属性中 没有和字典的key值匹配时 ---------------
- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
    //[super setValue:value forUndefinedKey:key];
    
}



- (void)setValue:(id)value forKey:(NSString *)key
{
    
    [super setValue:value forKey:key];
    //  if ([key isEqualToString:@"name"]) {
    
    // self.name = [NSString stringWithFormat:@"评论:%@", value];
    //  }
    
}

@end
