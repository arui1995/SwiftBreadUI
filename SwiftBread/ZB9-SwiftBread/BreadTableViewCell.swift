//
//  BreadTableViewCell.swift
//  ZB9-SwiftBread
//
//  Created by XuRui on 16/6/4.
//  Copyright © 2016年 XuRui. All rights reserved.
//

import UIKit
import Masonry
import SDWebImage

class BreadTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    /** 属性 */
    var imgUser : UIImageView?
    var labelUserName : UILabel?
    var labelContent : UILabel?
    var labeTime : UILabel?
    var labelName : UILabel?
    var heightContent : CGFloat?
    var arrWay : NSArray?
    var collectionView : UICollectionView?
    
    /** model赋值 */
    var model : TripModel? {
    
        set(newModel) {
        
            self.labelUserName?.text = newModel?.userName
            self.labelContent?.text = newModel?.trip_text
            self.labelName?.text = newModel?.name
            self.labeTime?.text = newModel?.datetime
            self.imgUser?.sd_setImageWithURL(NSURL.init(string: (newModel?.avatar_m)!))
            
            
            self.heightContent = newModel?.heightWithTripText
            self.arrWay = NSArray.init(array: (newModel?.waypoints)!)
            self.collectionView?.reloadData()
        }
        get {
            
        return self.model!
        }
    }

    
    /** 初始化 */
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        creatSubViews()
        creatCollectionView()
    }
    
  
    
    /** 创建子视图 */
    func creatSubViews() {
    
        self.imgUser = UIImageView.init()
        self.contentView.addSubview(imgUser!)
        imgUser?.layer.cornerRadius = 25
        imgUser?.clipsToBounds = true
        
        
        self.labelUserName = UILabel.init()
        self.contentView.addSubview(labelUserName!)
        labelUserName?.font = UIFont.systemFontOfSize(15)
        
        
        self.labelName = UILabel.init()
        self.contentView.addSubview(labelName!)
        labelName?.textColor = UIColor.init(colorLiteralRed: 0.41, green: 0.67, blue: 0.65, alpha: 1.00)
        labelName?.font = UIFont.systemFontOfSize(14)
        
        
        self.labeTime = UILabel.init()
        self.contentView.addSubview(labeTime!)
        labeTime?.textColor = UIColor.lightGrayColor()
        labeTime?.font = UIFont.systemFontOfSize(16)
        labeTime?.textAlignment = NSTextAlignment.Right
        

        self.labelContent = UILabel.init()
        self.contentView.addSubview(labelContent!)
        labelContent?.font = UIFont.systemFontOfSize(15)
        labelContent?.numberOfLines = 0
        labelContent?.textColor = UIColor.grayColor()

        //labelContent?.backgroundColor = UIColor.yellowColor()

    }
    
    
     /** 赋大小 */
    override func layoutSubviews() {
     
        super.layoutSubviews()
       configMasonry()
    }
    
    func configMasonry() {
    
        imgUser?.mas_makeConstraints({ (make : MASConstraintMaker!) in
           
            make.top.left().mas_equalTo()(self.contentView).offset()(10)
            make.width.height().mas_equalTo()(50)
            
            
        })
        
        labelUserName?.mas_makeConstraints({ (make: MASConstraintMaker!) in
            
            make.top.mas_equalTo()(self.imgUser)
            make.left.mas_equalTo()(self.imgUser!.mas_right).offset()(10)
            make.width.mas_equalTo()(200)
            make.height.mas_equalTo()(25)
            
        })
        
        labeTime?.mas_makeConstraints({ (make: MASConstraintMaker!) in
            
            make.top.mas_equalTo()(self.labelUserName)
            make.right.mas_equalTo()(-10)
            make.height.mas_equalTo()(25)
            make.left.mas_equalTo()(self.labelUserName!.mas_right).offset()(5)
           
        })
        
        labelName?.mas_makeConstraints({ (make: MASConstraintMaker!) in
            
            make.top.mas_equalTo()(self.labelUserName!.mas_bottom).offset()(0)
            make.left.height().mas_equalTo()(self.labelUserName)
            make.right.mas_equalTo()(-10)
        })
        
         /** 如果有文字值 */
        if (self.labelContent?.text != "") {
            
            labelContent?.hidden = false
            labelContent?.mas_remakeConstraints({ (make: MASConstraintMaker!) in
                
                make.top.mas_equalTo()(self.imgUser!.mas_bottom).offset()(10)
                make.left.mas_equalTo()(self.contentView).offset()(10)
                make.right.mas_equalTo()(self.contentView).offset()(-10)
                make.height.mas_equalTo()(self.heightContent!)
     
            })
            
            collectionView?.mas_remakeConstraints({ (make: MASConstraintMaker!) in
                
                make.top.mas_equalTo()(self.labelContent!.mas_bottom).offset()(10)
                make.left.right().mas_equalTo()(self.contentView).offset()(0)
                make.height.mas_equalTo()(120)
            })
            
        }else{
            
            labelContent?.hidden = true
            collectionView?.mas_remakeConstraints({ (make: MASConstraintMaker!) in
                
                make.top.mas_equalTo()(self.imgUser!.mas_bottom).offset()(10)
                make.left.right().mas_equalTo()(self.contentView).offset()(0)
                make.height.mas_equalTo()(120)
            })
        }
        
    }
    
    
    /** 创建collection */
    func creatCollectionView() {
        
        let layout : UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 10
        layout.scrollDirection = UICollectionViewScrollDirection.Horizontal
        
        self.collectionView = UICollectionView.init(frame: self.contentView.bounds, collectionViewLayout: layout)
     collectionView?.backgroundColor = UIColor.init(colorLiteralRed: 0.98, green: 0.96, blue: 0.93, alpha: 1.00)
    
        collectionView?.delegate = self
        collectionView?.dataSource = self
        self.contentView.addSubview(collectionView!)
        collectionView?.registerClass(BreadCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    /** 返回cell赋值 */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: BreadCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! BreadCollectionViewCell
        cell.pModel = self.arrWay![indexPath.item] as? PhotoModel
        
        return cell
    }
    
    /** 返回个数 */
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrWay!.count
    }
    
    /** 返回item大小 */
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let model : PhotoModel = (self.arrWay![indexPath.item] as? PhotoModel)!
        
        return CGSizeMake(model.pW!, 100)
    
    }
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    /** 当前类 实现异于父类的初始化方法 */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
