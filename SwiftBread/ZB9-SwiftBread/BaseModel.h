//
//  BaseModel.h
//  TasteEnjoy
//
//  Created by XuRui on 16/4/19.
//  Copyright © 2016年 XuRui. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary;

+ (NSMutableArray *)modelHandleWithArrar:(NSArray *)arr;


@end
