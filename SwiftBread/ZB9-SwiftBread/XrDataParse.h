//
//  XrDataParse.h
//  TasteEnjoy
//
//  Created by XuRui on 16/4/19.
//  Copyright © 2016年 XuRui. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>


@interface XrDataParse : NSObject

+ (CGFloat)heghtWithImageName:(NSString *)imageName widthWithOriginal:(NSNumber *)widthOriginal heightWithOriginal:(NSNumber *)heightOriginal heightWithNow:(CGFloat)heightNow;


+ (CGSize)heightWithText:(NSString *)text LabelWithSize:(CGSize)size fontText:(UIFont *)font;
@end
