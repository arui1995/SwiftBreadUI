//
//  ViewController.swift
//  ZB9-SwiftBread
//
//  Created by XuRui on 16/6/4.
//  Copyright © 2016年 XuRui. All rights reserved.
//  gitHub: 



import UIKit
//数据解析
import Alamofire


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //属性
    var tableView : UITableView?
    var arrData : NSMutableArray?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //数组初始化
        self.arrData = NSMutableArray()
        
        dataHandler()
        creatTableView()
        
        
    }

    /** 数据处理 */
    func dataHandler() {
    
        Alamofire.request(.GET, "http://api.breadtrip.com/destination/place/5/2388503755/poi_trips/?start=0&count=100").responseJSON{(result:Response<AnyObject, NSError>) in
        
            //最外层字典
            let resultDic : NSDictionary = result.result.value as!NSDictionary
            
            //取要的数组
            let arr : NSArray = resultDic["trips"] as! NSArray
            //baseModel 遍历arr
            self.arrData! = (TripModel.modelHandleWithArrar(arr as [AnyObject]))
            self.tableView?.reloadData()
        }
    
    }
    
    
    /** 创建tableView */
    func creatTableView() {
    
        self.tableView = UITableView.init(frame: UIScreen.mainScreen().bounds, style: UITableViewStyle.Plain)
        tableView?.contentInset = UIEdgeInsetsMake(30, 0, 0, 0)
        self.view.addSubview(tableView!)
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.registerClass(BreadTableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    /** 返回个数 */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrData!.count
    }
    
    /** 返回cell赋值 */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
    let cell : BreadTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as!BreadTableViewCell
      cell.backgroundColor = (UIColor.init(colorLiteralRed: 0.98, green: 0.96, blue: 0.93, alpha: 1.00))
       cell.model = self.arrData![indexPath.row] as? TripModel

        return cell
    }
    
    
    
 /** 返回高度 */
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let m = self.arrData![indexPath.row] as? TripModel
        
        //没有文字返回的高度
        if (m!.trip_text == "")  {
            
           return 190
        }else {
        return (m?.heightWithTripText)! + 200
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    
}

