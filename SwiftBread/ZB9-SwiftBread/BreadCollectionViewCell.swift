//
//  BreadCollectionViewCell.swift
//  ZB9-SwiftBread
//
//  Created by XuRui on 16/6/4.
//  Copyright © 2016年 XuRui. All rights reserved.
//

import UIKit
import SDWebImage
class BreadCollectionViewCell: UICollectionViewCell {
    
    //属性
    var img : UIImageView?
    
    //赋值
    var pModel : PhotoModel?{
    
        set (newModel){
        
            img?.sd_setImageWithURL(NSURL.init(string: (newModel?.photo)!))
            
        }
        get {
    
            return self.pModel!
        }
        
    
    }
   
    //初始化 方法
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        creatSub()
    }
    
    func creatSub() {
    
    
        self.img = UIImageView.init()
        self.contentView.addSubview(img!)
        
    
    }
    
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        
        img?.frame = self.bounds
        
    }
    
    
    
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
