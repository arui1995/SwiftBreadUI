//
//  PhotoModel.swift
//  ZB9-SwiftBread
//
//  Created by XuRui on 16/6/4.
//  Copyright © 2016年 XuRui. All rights reserved.
//

import UIKit

class PhotoModel: BaseModel {

    
    var photo : String?
    var h : NSNumber?
    var w : NSNumber?
    var pW: CGFloat?
    
    //重写 容错方法
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
        if key == "photo_info" {
            
            self.h = value!["h"] as? NSNumber
            self.w = value!["w"] as? NSNumber

            let w : CGFloat?
            
            //图片宽度比例 在这算好
            w = XrDataParse.heghtWithImageName(self.photo, widthWithOriginal: self.w, heightWithOriginal: self.h, heightWithNow: 100)
            
            self.pW = w
        }
    }
}


